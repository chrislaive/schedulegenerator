export const state = () => ({
    horarios: []
})

export const mutations = {
    addCurso(state){
        //state.cantidadCursos++;
    },
    addHorarios(state,val){
        state.horarios.push(val);
    },
    fillHorarios(state,val){
        state.horarios=val.slice();
    }
}

export const actions = {
    addCurso({commit}){
        commit('addCurso');
    },
    async updateHorarios({commit}){
        const res = await fetch(`${process.env.baseUrl}/api/horarios/last`);
        const data = await res.json();
        commit('fillHorarios',data)
    },
    async nuxtServerInit({ commit }){
        const res = await fetch(`${process.env.baseUrl}/api/horarios/last`);
        const data = await res.json();
        commit('fillHorarios',data);
    }
}

export const getters = {
    getEscuelas: state => {
        let escuelas = []
        for(let key in state.horarios){
            escuelas.push(state.horarios[key].escuela)
        }
        return escuelas
    },
    getSemestre: state => {
        return state.horarios[0].semestre
    },
    getHorariosXCiclos: state => escuela => {
        let horarios = state.horarios.filter(horario => horario.escuela==escuela)
        return horarios;
    },
    getCiclosXEscuela: state => escuela => {
        let ciclos = []
        for(let key in state.horarios){
            if(escuela==state.horarios[key].escuela){
                for(let val in state.horarios[key].ciclos){
                    ciclos.push(state.horarios[key].ciclos[val].title)
                }
                return ciclos;
            }
        }
    },
    getCursosXCicloXEscuela: state => (escuela,ciclo) => {
        let cursos = []
        for(let key in state.horarios){
            if(escuela==state.horarios[key].escuela){
                for(let val in state.horarios[key].ciclos){
                    if(ciclo==state.horarios[key].ciclos[val].title){
                        for(let band in state.horarios[key].ciclos[val].cursos){
                            for(let band1 in state.horarios[key].ciclos[val].cursos[band].planes){
                                if(!cursos.includes(state.horarios[key].ciclos[val].cursos[band].planes[band1].nombreCurso)){
                                    cursos.push(state.horarios[key].ciclos[val].cursos[band].planes[band1].nombreCurso)
                                }
                                

                                // cursos.push({nombre:state.horarios[key].ciclos[val].cursos[band].planes[band1].nombreCurso,plan:state.horarios[key].ciclos[val].cursos[band].planes[band1].title})
                            }
                        }
                        return cursos;
                    }
                }
            }
        }
    },
    getCursosXCicloXEscuela1: state => (escuela,ciclo) => {
        let cursos = []
        for(let key in state.horarios){
            if(escuela==state.horarios[key].escuela){
                for(let val in state.horarios[key].ciclos){
                    if(ciclo==state.horarios[key].ciclos[val].title){
                        for(let band in state.horarios[key].ciclos[val].cursos){
                            for(let band1 in state.horarios[key].ciclos[val].cursos[band].planes){
                                cursos.push({nombre:state.horarios[key].ciclos[val].cursos[band].planes[band1].nombreCurso,plan:state.horarios[key].ciclos[val].cursos[band].planes[band1].title})
                            }
                        }
                        return cursos;
                    }
                }
            }
        }
    },
    getGruposXCiclosXEscuelaXCurso: state => (escuela,ciclo,curso) => {
        let grupos = [];
        for(let key in state.horarios){
            if(escuela==state.horarios[key].escuela){
                for(let val in state.horarios[key].ciclos){
                    if(ciclo==state.horarios[key].ciclos[val].title){
                        for(let band in state.horarios[key].ciclos[val].cursos){
                            let test = false;
                            for(let band1 in state.horarios[key].ciclos[val].cursos[band].planes){
                                if(curso==state.horarios[key].ciclos[val].cursos[band].planes[band1].nombreCurso){
                                    test=true;
                                    break;
                                }
                            }
                            if(test){
                                for(let band1 in state.horarios[key].ciclos[val].cursos[band].grupos){
                                    if(!grupos.includes(state.horarios[key].ciclos[val].cursos[band].grupos[band1].title)){
                                        grupos.push(state.horarios[key].ciclos[val].cursos[band].grupos[band1].title)
                                    }
                                }
                            }
                        }
                        return grupos;
                    }
                }
            }
        }
    },
    getPlanesXCicloXEscuela: state=> (escuela,ciclo) => {
        let planes = [];
        for(let key in state.horarios){
            if(escuela==state.horarios[key].escuela){
                for(let val in state.horarios[key].ciclos){
                    if(ciclo==state.horarios[key].ciclos[val].title){
                        for(let band in state.horarios[key].ciclos[val].cursos){
                            for(let band1 in state.horarios[key].ciclos[val].cursos[band].planes){
                                if(!planes.includes(state.horarios[key].ciclos[val].cursos[band].planes[band1].title)){
                                    planes.push(state.horarios[key].ciclos[val].cursos[band].planes[band1].title)
                                }
                            }
                        }
                        return planes.sort();
                    }
                }
            }
        }
    },
    getPlanesXCicloXEscuelaXCurso: state => (escuela,cicloS,curso) => {
        let planes = [];
        const horario = state.horarios.filter(horario=>horario.escuela==escuela)[0];
        const ciclo = horario.ciclos.filter(ciclo=>ciclo.title==cicloS)[0]
        for(let key in ciclo.cursos){
            for(let val in ciclo.cursos[key].planes){
                if(ciclo.cursos[key].planes[val].nombreCurso==curso){
                    planes.push(ciclo.cursos[key].planes[val].title)
                }
            }
        }
        return planes.sort();
    },
    getGruposXPlanesXCiclosXEscuela: state => (escuela,ciclo,plan) => {
        let grupos = [];
        for(let key in state.horarios){
            if(escuela==state.horarios[key].escuela){
                for(let val in state.horarios[key].ciclos){
                    if(ciclo==state.horarios[key].ciclos[val].title){
                        for(let band in state.horarios[key].ciclos[val].cursos){
                            let test = false;
                            for(let band1 in state.horarios[key].ciclos[val].cursos[band].planes){
                                if(plan==state.horarios[key].ciclos[val].cursos[band].planes[band1].title){
                                    test=true;
                                    break;
                                }
                            }
                            if(test){
                                for(let band1 in state.horarios[key].ciclos[val].cursos[band].grupos){
                                    if(!grupos.includes(state.horarios[key].ciclos[val].cursos[band].grupos[band1].title)){
                                        grupos.push(state.horarios[key].ciclos[val].cursos[band].grupos[band1].title)
                                    }
                                }
                            }
                        }
                        return grupos;
                    }
                }
            }
        }
    },
    getHorariosXEscuelaXCicloXGrupo: state => (escuela,cicloS,grupo) => {
        let cursos = [];
        const horarios = state.horarios.filter(horario => horario.escuela==escuela)[0]
        const ciclos = horarios.ciclos.filter(ciclo => ciclo.title==cicloS)[0]
        for(let key in ciclos.cursos){
            let band = false;
            for(let val in ciclos.cursos[key].planes){
                if(ciclos.cursos[key].planes[val].nombreCurso==grupo){
                    band=true;
                }
            }
            if(band){
                cursos.push(ciclos.cursos[key])
            }
        }
        return cursos[0];
    },
    getSchedule: state => (escuela,ciclo,plan,grupo) =>{
        let cursos = [];
        for(let key in state.horarios){
            if(escuela==state.horarios[key].escuela){
                for(let val in state.horarios[key].ciclos){
                    if(ciclo==state.horarios[key].ciclos[val].title){
                        for(let band in state.horarios[key].ciclos[val].cursos){
                            let nombreCurso='',vacantes='', tipoCurso='', horaInicio='', horaFinal='', profesor='', dia='', test = false;
                            for(let band1 in state.horarios[key].ciclos[val].cursos[band].planes){
                                if(plan==state.horarios[key].ciclos[val].cursos[band].planes[band1].title){
                                    test=true;
                                    nombreCurso=state.horarios[key].ciclos[val].cursos[band].planes[band1].nombreCurso;
                                    vacantes=state.horarios[key].ciclos[val].cursos[band].planes[band1].vacantes;
                                    break;
                                }
                            }
                            if(test){
                                for(let band1 in state.horarios[key].ciclos[val].cursos[band].grupos){
                                    if(grupo==state.horarios[key].ciclos[val].cursos[band].grupos[band1].title){
                                        for(let band2 in state.horarios[key].ciclos[val].cursos[band].grupos[band1].horario){
                                            tipoCurso=state.horarios[key].ciclos[val].cursos[band].grupos[band1].horario[band2].titulo;
                                            if(tipoCurso=='T'){
                                                tipoCurso='Teoría';
                                            } else {
                                                tipoCurso=='P'?tipoCurso='Práctica':tipoCurso='Laboratorio';
                                            }
                                            horaInicio=state.horarios[key].ciclos[val].cursos[band].grupos[band1].horario[band2].horaInicio;
                                            if(horaInicio<10){
                                                horaInicio='0'+horaInicio+':00'
                                            } else {
                                                horaInicio=horaInicio+':00'
                                            }
                                            horaFinal=state.horarios[key].ciclos[val].cursos[band].grupos[band1].horario[band2].horaFinal;
                                            if(horaFinal<10){
                                                horaFinal='0'+horaFinal+':00'
                                            } else {
                                                horaFinal=horaFinal+':00'
                                            }
                                            dia=state.horarios[key].ciclos[val].cursos[band].grupos[band1].horario[band2].dia;
                                            profesor=state.horarios[key].ciclos[val].cursos[band].grupos[band1].horario[band2].profesor;
                                            cursos.push({nombreCurso,tipoCurso,horaInicio,horaFinal,dia,profesor,vacantes});
                                        }
                                    }
                                }
                            }
                        }
                        if(plan=='' || plan==null || grupo=='' || grupo==null){
                            return null;
                        } else {
                            return cursos;
                        }
                    }
                }
            }
        }
    }
}