export const state = () => ({
    ciclo: null,
    plan: null,
    escuela: null,
    grupo: null
})

export const mutations = {
    updateCiclo(state,val){
        state.ciclo=val
    },
    updatePlan(state,val){
        state.plan=val
    },
    updateEscuela(state,val){
        state.escuela=val;
    },
    updateGrupo(state,val){
        state.grupo=val;
    }
}

export const getters = {
    getEscuela: state => {
        return state.escuela==null?null:state.escuela.toUpperCase();
    },
    getGrupo: state => {
        return state.grupo==null?null:state.grupo.slice(-1);
    }
}