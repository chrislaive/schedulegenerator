import { Router } from 'express';
import apiHorarios from './apiHorarios';
import apiProfesores from './apiProfesores';
import apiSemestres from './apiSemestre';

const app = Router();

app.use('/horarios',apiHorarios);
app.use('/profesores',apiProfesores);
app.use('/semestres',apiSemestres);

export default app