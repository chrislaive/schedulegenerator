import { Router } from 'express';
import Semestre from '../models/semestreSchema';

const app = Router();

app.get('/', async (req, res) => {
    const semestres = await Semestre.find({});
    res.json(semestres);
});

app.get('/last', async (req, res) => {
    const semestres = await Semestre.find();
    let semestre,b = '';
    for(let i in semestres){
        if(b<semestres[i].semestre){
            b=semestres[i].semestre
            semestre=semestres[i]
        }
    }  
    res.json(semestre);
})

app.post('/', async (req, res) => {
    const semestre = new Semestre(req.body);
    await semestre.save();
    res.json({status: 'semestre guardado'})
});

app.put('/:id', async (req, res) => {
    const semestre = await Semestre.findById(req.params.id);
    //logic for edit
    await semestre.save();
    res.json({status: 'semestre actualizado'})
});

app.delete('/:id', async (req, res) => {
    await Semestre.findByIdAndDelete(req.params.id);
    res.json({status: 'semestre eliminado'})
});

export default app;