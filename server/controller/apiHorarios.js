import { Router } from 'express';
import Horario from '../models/horarioSchema';

const app = Router();

app.get('/', async (req, res)=>{
    const horarios = await Horario.find({});
    res.json(horarios);
});

app.get('/last', async (req, res) => {
    const horarios = await Horario.find({});
    let horarioLast=[],b = '';
    for(let i in horarios){
        if(b<=horarios[i].semestre){
            b=horarios[i].semestre
            horarioLast.push(horarios[i])
        }
    }  
    res.json(horarioLast);
});

app.get('/planes/:escuela', async (req,res) => {
    const planes = await Horario.aggregate([
        {$match: {escuela:req.params.escuela}},
        {$project: {ciclos:1, _id:0} },
        {$unwind: "$ciclos"},
        {$replaceRoot: {newRoot: "$ciclos"}},
        {$project: {cursos:1, _id:0} },
        {$unwind: "$cursos"},
        {$replaceRoot: {newRoot: "$cursos"}},
        {$project: {planes:1, _id:0} },
        {$unwind: "$planes"},
        {$replaceRoot: {newRoot: "$planes"}},
        {$project: {title:1,_id:0} },
        {$group: {_id:null,uniqueValues:{$addToSet:"$title"}}},
        {$project:{uniqueValues:1,_id:0}}
    ])
    res.json(planes[0].uniqueValues.sort())
});

app.get('/:id', async (req, res) => {
    const horario = await Horario.findById(req.params.id);
    res.json(horario);
});

app.post('/addGrupo', async(req,res)=>{
    Horario.findOne({escuela:req.body.escuela},async function(err,horario){
        let horarioRaw = horario.ciclos.filter(ciclo=>ciclo.title==req.body.ciclo)[0];
        let horarioRaw1 = horarioRaw.cursos
        for(let key in horarioRaw1){
            let band=false;
            for(let val in horarioRaw1[key].planes){
                if(horarioRaw1[key].planes[val].nombreCurso==req.body.curso){
                    band=true;
                    for(let valIndex in req.body.vacantes){
                        if(horarioRaw1[key].planes[val].title == req.body.vacantes[valIndex].plan){
                            horarioRaw1[key].planes[val].vacantes.push(req.body.vacantes[valIndex].vacantes);
                        }
                    }
                    break;
                }
            }
            if(band){
                horarioRaw1[key].grupos.push(req.body.grupo);
                break;
            }
        }
        await horario.save();
        res.json({status:'Grupo agregado'});
    });
});

app.delete('/grupo/:id', async(req, res)=>{
    Horario.findOne({escuela:req.body.escuela}, async function(err,horario){
        let horarioRaw = horario.ciclos.filter(ciclo => ciclo.title==req.body.ciclo)[0];
        let horarioRaw1 = horarioRaw.cursos
        for(let key in horarioRaw1){
            let band=false;
            let index;
            for(let val in horarioRaw1[key].grupos){
                if(horarioRaw1[key].grupos[val]._id==req.params.id){
                    index=horarioRaw1[key].grupos[val].title.slice(-1)-1;
                    horarioRaw1[key].grupos.splice(val,1);
                    band=true;break;
                }
            }
            if(band){
                horarioRaw1[key].planes.filter(plan=>plan.vacantes.splice(index,1))
                break;
            }
        }
        await horario.save();
        res.json({status:'Grupo borrado.'})
    });
})

app.post('/', async (req, res) => {
    const horario = new Horario(req.body);
    await horario.save();
    res.json({
        status: 'Horario guardado'
    });
});

app.put('/:id', async (req, res) => {
    const horario = await Horario.findById(req.params.id);
    //logic for edit
    await horario.save();
    res.json({
        status: 'Horario actualizado'
    });
});

app.delete('/:id', async (req, res) => {
    await Horario.findByIdAndDelete(req.params.id);
    res.json({
        status: 'Horario borrado'
    });
});

export default app;