import { Router } from 'express';
import Profesor from '../models/profesSchema';

const app = Router();

app.get('/', async (req, res) => {
    const profesoress = await Profesor.find({});
    res.json(profesores);
});

app.post('/', async (req, res) => {
    const profesor = new Profesor(req.body);
    await profesor.save();
    res.json({status: 'profesor guardado'})
});

app.put('/:id', async (req, res) => {
    const profesor = await Profesor.findById(req.params.id);
    //logic for edit
    await profesor.save();
    res.json({status: 'profesor actualizado'})
});

app.delete('/:id', async (req, res) => {
    await Profesor.findByIdAndDelete(req.params.id);
    res.json({status: 'profesor eliminado'})
});

export default app;