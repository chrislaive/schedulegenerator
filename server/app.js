const express = require('express')
const consola = require('consola')
const { Nuxt, Builder } = require('nuxt')
import bodyParser from 'body-parser';
import controller from './controller';
import './database';
import secrets from './secret';

// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  
  const app = express()

  app.use(bodyParser.urlencoded({extended:false}));
  app.use(bodyParser.json());
  app.use('/api', controller)

  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  const { host, port } = nuxt.options.server

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  } else {
    await nuxt.ready()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()
