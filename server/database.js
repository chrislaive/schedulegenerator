import Mongoose from 'mongoose';
import secrets from './secret';

Mongoose.connect(`mongodb://${secrets.database.user}:${secrets.database.password}@${secrets.database.domain}/${secrets.database.name}`, {useCreateIndex: true, useNewUrlParser: true, useFindAndModify: false});
let db = Mongoose.connection;
db.on('err',console.error.bind(console,'Connection Error: '));
db.once('open', ()=>{
	console.log('Database is Connected!');
	return db;
})