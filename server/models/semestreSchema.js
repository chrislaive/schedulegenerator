import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const semestre = new Schema({
    semestre: String,
    horarios: [
        {
            id: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Horarios'
            }
        }
    ] 
});

export default mongoose.model('Semestres',semestre);