import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const profesor = new Schema({
    nombre: String,
    apellido: String,
    codigo: String,
    fotoUrl: String,
    cargaLectiva: Number,
    facultadOrigen: String,
    genero: String
});

export default mongoose.model('Profesores',profesor);