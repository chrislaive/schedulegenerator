import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const horario = new Schema({
    semestre: String,
    escuela: String,
    ciclos: [
        {
            title: Number,
            cursos: [
                {
                    grupos: [
                        {
                            title: String,
                            horario: [
                                {
                                    titulo: String,
                                    horaInicio: Number,
                                    horaFinal: Number,
                                    dia: String,
                                    profesor: String
                                }
                            ]
                        }
                    ],
                    planes: [
                        {
                            title: String,
                            nombreCurso: String,
                            vacantes: [Number]
                        }
                    ],
                    coordinador: String
                }
            ]
        }
    ]
});

export default mongoose.model('Horarios',horario);